OBJETIVO DE LA PRACTICA
Desarrollar una aplicación para ENCUESTAS que permita responder preguntas que pueden ser de 2 tipos: selección múltiple (1 respuesta correcta o múltiples respuestas correctas) y abiertas (texto libre), así como la opción de visualizar la información.
• ALCANCE 
a) La aplicación puede ser web o de escritorio. 
b) Deberá contar con un acceso público para la captura de la encuesta y un acceso con usuario y contraseña para consultar todas las encuestas capturadas. 
c) Mínimo 8 preguntas, el tema es de libre elección. 
d) Las respuestas deberán utilizar los controles: checkbox, radiobutton, textboxy combobox/select (listas desplegables) 
e) No se deberá permitir al encuestado dejar ningún campo sin responder y se deberá validar que la información capturada coincida con el dato solicitado, p. e. el correo electrónico debe tener una estructura válida como encuesta@ejemplos.org. 
f) La aplicación deberá permitir el registro de información general del encuestado como: Nombre, Sexo, Edad, Teléfono, Correo electrónico, etc. 
g) Una vez completada la encuesta, ésta deberá enviarse una copia de la misma por correo electrónico a la dirección dada por el encuestado. 
h) La visualización de la encuesta respondida deberá hacerse en pantalla de consulta y exportarse formato PDF o XLSX (Excel) 